#
# Copyright (C) 2022 Paranoid Android
#
# SPDX-License-Identifier: Apache-2.0
#

LOCAL_PATH := $(call my-dir)

ifeq ($(PRODUCT_DEVICE),udon)

$(info Including firmware for udon...)

$(call add-radio-file-sha1-checked,images/abl.img,e3c6ef8a359c9dafee206e1bd40c0fa8318f7dd9)
$(call add-radio-file-sha1-checked,images/aop.img,fe9c58275e39532838398762f106b4ae50d1224d)
$(call add-radio-file-sha1-checked,images/aop_config.img,ec9efb7b7c6c2718611b3a4d4e7cbd3c15c3a04c)
$(call add-radio-file-sha1-checked,images/bluetooth.img,4fa25d5850d11b355974d29ac9dc1fe2130f260b)
$(call add-radio-file-sha1-checked,images/cpucp.img,0018e566b331df419e0cac35318c02f1613442f4)
$(call add-radio-file-sha1-checked,images/devcfg.img,e809caa692c7b10cf56993ac0b74e6e255bcdd7a)
$(call add-radio-file-sha1-checked,images/dsp.img,3494365be3625d15d6d748eccc02c279682a54e5)
$(call add-radio-file-sha1-checked,images/engineering_cdt.img,6b16b5041885f1d3d3a755a8794808b25c893015)
$(call add-radio-file-sha1-checked,images/featenabler.img,88a7d7449c5b396d045371cbe02cdda6041c9e40)
$(call add-radio-file-sha1-checked,images/hyp.img,7badcc30ea3b0435f9e00aec8536b2b01e2ec58b)
$(call add-radio-file-sha1-checked,images/imagefv.img,70b5114abe71bdaec0aef9dca3ff95225e222666)
$(call add-radio-file-sha1-checked,images/keymaster.img,306da84915811cf3d243d21ed444d4b7d712b5d9)
$(call add-radio-file-sha1-checked,images/modem.img,9e997448834c95b8bd392cab551e623e2106e7bd)
$(call add-radio-file-sha1-checked,images/oplus_sec.img,0a42b00f898f24513f93ced9ad296f40d3ed8fd5)
$(call add-radio-file-sha1-checked,images/oplusstanvbk.img,51712ab4a60063b4b9243e2a3a564d0f90acd5f5)
$(call add-radio-file-sha1-checked,images/qupfw.img,1b84b033235c8151680c69e6a1656bde783b04b6)
$(call add-radio-file-sha1-checked,images/shrm.img,7a00cdfe306bca477a7ad8377ba8f3c167db9598)
$(call add-radio-file-sha1-checked,images/splash.img,096eb53fde3c3574925bf30b08fc95a5aa7ee35b)
$(call add-radio-file-sha1-checked,images/tz.img,044834197a1790b4d64959a1513a1c2827a11eae)
$(call add-radio-file-sha1-checked,images/uefi.img,68a203b4c98d856f0b89f8c0494b8e67406cffef)
$(call add-radio-file-sha1-checked,images/uefisecapp.img,6c8d9983a099194a8b0f483b0792172084a7f921)
$(call add-radio-file-sha1-checked,images/xbl.img,5982821f565ec51bf6363f5564f33a51fded99b2)
$(call add-radio-file-sha1-checked,images/xbl_config.img,70cea3e16ceffe480172c08505ac68f991e897db)
$(call add-radio-file-sha1-checked,images/xbl_ramdump.img,e20c9682ab3a22fe5bfa804109fba75ec7c38679)

endif
